
library(exams)

#alterar a regra de cálculo da pontuação
make.mchoice.all <- make_question_moodle(eval = list(rule = "all")) 
set.seed(1)
exams::exams2moodle(file = "mchoice-rdm.Rmd",
                    n = 1,
                    edir = "example",
                    dir = "example",
                    mchoice = make.mchoice.all,
                    name = "avalia_all")



#alterar a regra de cálculo da pontuação
make.mchoice.false <- make_question_moodle(eval = list(rule = "false")) 
set.seed(1)
exams::exams2moodle(file = "mchoice-rdm.Rmd",
                    n = 1,
                    edir = "example",
                    dir = "example",
                    mchoice = make.mchoice.false,
                    name = "avalia_false")


#alterar a regra de cálculo da pontuação
make.mchoice.none <- make_question_moodle(eval = list(rule = "none")) 
set.seed(1)
exams::exams2moodle(file = "mchoice-rdm.Rmd",
                    n = 1,
                    edir = "example",
                    dir = "example",
                    mchoice = make.mchoice.none,
                    name = "avalia_none")


#alterar a regra de cálculo da pontuação
make.mchoice.true <- make_question_moodle(eval = list(rule = "true")) 
set.seed(1)
exams::exams2moodle(file = "mchoice-rdm.Rmd",
                    n = 1,
                    edir = "example",
                    dir = "example",
                    mchoice = make.mchoice.true,
                    name = "avalia_true")