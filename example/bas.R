library(exams)

ex.bas <- c("schoice.Rmd",
            "mchoice.Rmd",
            "string.Rmd",
            "num.Rmd")

exams::exams2moodle(file = ex.bas,
                    edir="example",
                    dir="example",
                    name="bas")


