Question
========
Sobre o Brasil, qual das seguintes afirmativas está correta?

Answerlist
----------
* A bandeira do Brasil tem a cor verde.
* O idioma oficial do Brasil é o brasileiro.
* A moeda atual do Brasil é o cruzeiro.
* O Brasil está na América do Norte.
* A capital do Brasil é Buenos Aires.

Solution
========
Answerlist
----------
* CORRETA. 
* FALSA. O idioma oficial do Brasil é o português.
* FALSA. A moeda atual do Brasil é o Real.
* FALSA. O Brasil está na América do Sul.
* FALSA. A capital do Brasil é Brasília.

Meta-information
================
exname: Brasil
extype: schoice
exsolution: 10000
exshuffle: TRUE